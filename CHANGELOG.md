## [0.1.6] - 2021-05-20

Try to avoid page breaks in text and other elements

## [0.1.5] - 2021-05-19

Style fixes

## [0.1.4] - 2021-05-19

Make print automatic, fix bug

## [0.1.3] - 2021-05-19

Button fix

## [0.1.2] - 2021-05-19

Build system updated

## [0.1.1] - 2021-05-19

Build system updated

## [0.1.0] - 2021-05-19

Test release
